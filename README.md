# API BileMo

## Prérequis
Avoir un hébergeur de base de données (Wamp, Xamp..) et composer


## Installation
- Cloner le projet: `git clone https://gitlab.com/Isoware/api-bilemo.git`
- Lancer `composer update`
- Installer la base de données avec le fichier "bilemo.sql" (changer la variable "DATABASE_URL" dans le .env)
- Lancer le serveur


## Utilisation
URL de base: http://127.0.0.1:8000/api/

Exemple d'utilisation: http://127.0.0.1:8000/api/user

### Login
`http://127.0.0.1:8000/api/login_check` (retourne JWT token)

### Utilisateurs
#### Attributs
- id (int)
- name (string)
- surname (string)
- username (string)
- password (string)
- mail (string)
- adress (array)
- roles (array)
#### URLS
- Ajout (Méthode: POST) `user`
- Supression (Méhode: DELETE) `user/{id}`
- Voir un utilisateur (Méthode: GET) `user/{id}`
- Liste des utilisateurs (Méthode: GET) `users`

### Produits
#### Attributs
- id (int)
- name (string)
- description (string)
- price (float)
#### URLS
- Ajout (Méthode: POST) `produit`
- Supression (Méhode: DELETE) `produit/{id}`
- Voir un produit (Méthode: GET) `produit/{id}`
- Liste des produits (Méthode: GET) `produits`
- Modification (Méthode: PATCH) `produit/{id}`
