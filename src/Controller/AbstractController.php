<?php

namespace App\Controller;

use OpenApi\Annotations as OA;

/**
 * @OA\Response(
 *     response="NotFound",
 *     description="La ressource n'existe pas",
 *     @OA\JsonContent(
 *         @OA\Property(property="message", type="string")
 *     )
 * )
 * @OA\Response(
 *     response="BadRequest",
 *     description="Les données envoyées ne sont pas celles attendues",
 *     @OA\JsonContent(
 *         @OA\Property(property="message", type="string"),
 *         @OA\Property(property="erreurs", type="array", @OA\Items(@OA\Property(property="value", type="string")))
 *     )
 * )
 *
 * @OA\Parameter(
 *     name="id",
 *     in="path",
 *     description="ID de la ressource",
 *     required=true,
 *     @OA\Schema(type="integer")
 * )
 * @OA\Parameter(
 *     name="page",
 *     in="path",
 *     description="Page désirée",
 *     required=true,
 *     @OA\Schema(type="integer")
 * )
 *
 * @OA\SecurityScheme(bearerFormat="JWT", type="apiKey", securityScheme="bearer")
 */
class AbstractController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{

}
