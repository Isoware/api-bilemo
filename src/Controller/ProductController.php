<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;


class ProductController extends AbstractController
{
    /**
     * @param SerializerInterface $serializer
     * @param int $id
     * @param ProductRepository $productRepo
     * @return Response
     * @OA\Get(
     *     path="/product/{id}",
     *     tags={"Produit"},
     *     security={"bearer"},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *         response="200",
     *         description="Description d'un produit",
     *         @OA\JsonContent(@OA\Items(ref="#/components/schemas/Product")),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     )
     * )
     */
    #[Route('/api/product/{id}', name: 'product.show', methods: ['GET'])]
    public function show(SerializerInterface $serializer, int $id, ProductRepository $productRepo): Response
    {
        $product = $productRepo->find($id);

        if(!$product) {
            $response = new JsonResponse(['message' => 'Le produit n\'existe pas'], Response::HTTP_NOT_FOUND);
        } else {
            $data = $serializer->serialize($product, 'json');
            $response = new Response($data, Response::HTTP_OK);
        }

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ValidatorInterface $validator
     * @return Response
     * @OA\Post(
     *     path="/product",
     *     tags={"Produit"},
     *     security={"bearer"},
     *     @OA\RequestBody(ref="#/components/requestBodies/ProductRequestBody"),
     *     @OA\Response(
     *         response="201",
     *         description="Création d'un produit"
     *     ),
     *     @OA\Response(
     *         response="400",
     *         ref="#/components/responses/BadRequest"
     *     )
     * )
     */
    #[Route('/api/product', name: 'product.create', methods: ['POST'])]
    public function create(Request $request, SerializerInterface $serializer, ValidatorInterface $validator): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $product = new Product();
        foreach($data as $property=>$value) {
            if (property_exists($product, $property)) {
                $product->{'set'.ucfirst($property)}($value);
            }
        }

        $errors = $validator->validate($product);

        if (count($errors)) {
            $errorMessages = [];
            foreach($errors as $error) {
                $errorMessages[$error->getPropertyPath()] = $error->getMessage();
            }

            return new JsonResponse([
                'message' => 'Les données envoyées ne sont pas celles attendues',
                'erreurs' => $errorMessages
            ], Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();

        $data = $serializer->serialize($product, 'json');

        $response = new Response($data, Response::HTTP_CREATED);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param ProductRepository $productRepo
     * @param SerializerInterface $serializer
     * @return Response
     * @OA\Get(
     *     path="/products",
     *     tags={"Produit"},
     *     security={"bearer"},
     *     @OA\Response(
     *         response="200",
     *         description="Liste des produits",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Product")),
     *     )
     * )
     */
    #[Route('/api/products', name: 'product.list', methods: ['GET'])]
    public function list(ProductRepository $productRepo, SerializerInterface $serializer): Response
    {
        $products = $productRepo->findAll();
        $data = $serializer->serialize($products, 'json');

        $response = new Response($data, Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param ProductRepository $productRepo
     * @param SerializerInterface $serializer
     * @param $page
     * @return Response
     * @OA\Get(
     *     path="/products/{page}",
     *     tags={"Produit"},
     *     security={"bearer"},
     *     @OA\Parameter(ref="#/components/parameters/page"),
     *     @OA\Response(
     *         response="200",
     *         description="Liste des 5 produits de la page spécifiée",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Product")),
     *     )
     * )
     */
    #[Route('/api/products/{page}', name: 'product.listPage', methods: ['GET'])]
    public function listPage(ProductRepository $productRepo, SerializerInterface $serializer, $page): Response
    {
        $products = $productRepo->findPage($page);
        $data = $serializer->serialize($products, 'json');

        $response = new Response($data, Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param int $id
     * @param ProductRepository $productRepo
     * @return Response
     * @OA\Delete(
     *     path="/product/{id}",
     *     tags={"Produit"},
     *     security={"bearer"},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *         response="204",
     *         description="Le produit a été supprimé"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     )
     * )
     */
    #[Route('/api/product/{id}', name: 'product.delete', methods: ['DELETE'])]
    public function delete(int $id, ProductRepository $productRepo): Response
    {
        $product = $productRepo->find($id);

        if(!$product) {
            $response = new JsonResponse(['message' => 'Le produit n\'existe pas'], Response::HTTP_NOT_FOUND);
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();

            $response = new JsonResponse([], Response::HTTP_NO_CONTENT);
        }

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param Request $request
     * @param int $id
     * @param ProductRepository $productRepo
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @return Response
     *
     * @OA\Patch(
     *     path="/product/{id}",
     *     tags={"Produit"},
     *     security={"bearer"},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\RequestBody(ref="#/components/requestBodies/ProductRequestBody"),
     *     @OA\Response(
     *         response="200",
     *         description="Mise à jour d'un produit",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Product")),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     )
     * )
     */
    #[Route('/api/product/{id}', name: 'product.update', methods: ['PATCH'])]
    public function update(Request $request, int $id, ProductRepository $productRepo, ValidatorInterface $validator, SerializerInterface $serializer): Response
    {
        $product = $productRepo->find($id);

        if(!$product) {
            $response = new JsonResponse(['message' => 'Le produit n\'existe pas'], Response::HTTP_NOT_FOUND);
        } else {
            $data = $request->getContent();
            $data = json_decode($data);

            foreach($data as $property=>$value) {
                if (property_exists($product, $property)) {
                    $product->{'set'.ucfirst($property)}($value);
                }
            }

            $errors = $validator->validate($product);

            if (count($errors)) {
                $errorMessages = [];
                foreach($errors as $error) {
                    $errorMessages[$error->getPropertyPath()] = $error->getMessage();
                }

                return new JsonResponse([
                    'message' => 'Les données envoyées ne sont pas celles attendues',
                    'erreurs' => $errorMessages
                ], Response::HTTP_BAD_REQUEST);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $data = $serializer->serialize($product, 'json');
            $response = new Response($data, Response::HTTP_OK);
        }

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
