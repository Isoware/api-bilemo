<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\User;
use App\Repository\ClientRepository;
use App\Repository\UserRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;

class UserController extends AbstractController
{
    /**
     * @param SerializerInterface $serializer
     * @param int $id
     * @param UserRepository $userRepo
     * @return Response
     * @OA\Get(
     *     path="/user/{id}",
     *     tags={"Utilisateur"},
     *     security={"bearer"},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *         response="200",
     *         description="Description d'un utilisateur",
     *         @OA\JsonContent(@OA\Items(ref="#/components/schemas/User")),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     )
     * )
     */
    #[Route('/api/user/{id}', name: 'user.show', methods: ['GET'])]
    public function show(SerializerInterface $serializer, int $id, UserRepository $userRepo): Response
    {
        $user = $userRepo->find($id);

        if(!$user) {
            $response = new JsonResponse(['message' => 'L\'utilisateur n\'existe pas'], Response::HTTP_NOT_FOUND);
        } else {
            $data = $serializer->serialize($user, 'json');
            $response = new Response($data, Response::HTTP_OK);
        }

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param UserPasswordHasherInterface $passwordHasher
     * @param Request $request
     * @param ClientRepository $clientRepo
     * @param ValidatorInterface $validator
     * @return Response
     * @OA\Post(
     *     path="/user",
     *     tags={"Utilisateur"},
     *     security={"bearer"},
     *     @OA\RequestBody(ref="#/components/requestBodies/UserRequestBody"),
     *     @OA\Response(
     *         response="201",
     *         description="Création d'un utilisateur"
     *     ),
     *     @OA\Response(
     *         response="400",
     *         ref="#/components/responses/BadRequest"
     *     )
     * )
     */
    #[Route('/api/user', name: 'user.create', methods: ['POST'])]
    public function create(UserPasswordHasherInterface $passwordHasher, Request $request, ClientRepository $clientRepo, ValidatorInterface $validator, SerializerInterface $serializer): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $user = new User();
        foreach($data as $property=>$value) {
            if (property_exists($user, $property)) {
                if($property == 'client') {
                    $client = $clientRepo->find($value);
                    if(!$client) {
                        return new JsonResponse(['message' => 'Le client n\'existe pas'], Response::HTTP_NOT_FOUND);
                    } else {
                        $user->setClient($client);
                    }
                    continue;
                }
                $user->{'set'.ucfirst($property)}($value);
            }
        }

        $errors = $validator->validate($user);

        if (count($errors)) {
            $errorMessages = [];
            foreach($errors as $error) {
                $errorMessages[$error->getPropertyPath()] = $error->getMessage();
            }

            return new JsonResponse([
                'message' => 'Les données envoyées ne sont pas celles attendues',
                'erreurs' => $errorMessages
            ], Response::HTTP_BAD_REQUEST);
        }

        $user->setPassword($passwordHasher->hashPassword($user, $user->getPassword()));

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $data = $serializer->serialize($user, 'json');
        $response = new Response($data, Response::HTTP_CREATED);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param int $id
     * @param UserRepository $userRepo
     * @return Response
     * @OA\Delete(
     *     path="/user/{id}",
     *     tags={"Utilisateur"},
     *     security={"bearer"},
     *     @OA\Parameter(ref="#/components/parameters/id"),
     *     @OA\Response(
     *         response="204",
     *         description="L'utilisateur a été supprimé"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     )
     * )
     */
    #[Route('/api/user/{id}', name: 'user.delete', methods: ['DELETE'])]
    public function delete(int $id, UserRepository $userRepo): Response
    {
        $user = $userRepo->find($id);

        if(!$user) {
            $response = new JsonResponse(['message' => 'L\'utilisateur n\'existe pas'], Response::HTTP_NOT_FOUND);
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            $response = new JsonResponse([], Response::HTTP_NO_CONTENT);
        }


        return $response;
    }

    /**
     * @param UserRepository $userRepo
     * @param SerializerInterface $serializer
     * @return Response
     * @OA\Get(
     *     path="/users",
     *     tags={"Utilisateur"},
     *     security={"bearer"},
     *     @OA\Response(
     *         response="200",
     *         description="Liste des produits",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Product")),
     *     )
     * )
     */
    #[Route('/api/users', name: 'user.list', methods: ['GET'])]
    public function list(UserRepository $userRepo, SerializerInterface $serializer): Response
    {
        $users = $userRepo->findAll();
        $data = $serializer->serialize($users, 'json');

        $response = new Response($data, Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param UserRepository $userRepo
     * @param SerializerInterface $serializer
     * @param $page
     * @return Response
     * @OA\Get(
     *     path="/users/{page}",
     *     tags={"Utilisateur"},
     *     security={"bearer"},
     *     @OA\Parameter(ref="#/components/parameters/page"),
     *     @OA\Response(
     *         response="200",
     *         description="Liste des 5 utilisateurs de la page spécifiée",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Product")),
     *     )
     * )
     */
    #[Route('/api/users/{page}', name: 'user.listPage', methods: ['GET'])]
    public function listPage(UserRepository $userRepo, SerializerInterface $serializer, $page): Response
    {
        $users = $userRepo->findPage($page);
        $data = $serializer->serialize($users, 'json');

        $response = new Response($data, Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @param int $id
     * @param ClientRepository $clientRepo
     * @param SerializerInterface $serializer
     * @return Response
     * @OA\Get(
     *     path="/users/{client}",
     *     tags={"Utilisateur"},
     *     security={"bearer"},
     *     @OA\Parameter(
     *         name="client",
     *         in="path",
     *         description="ID du client",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Liste des utilisateurs liés au client",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Product")),
     *     ),
     *     @OA\Response(
     *         response="404",
     *         ref="#/components/responses/NotFound"
     *     )
     * )
     */
    #[Route('/api/clientUsers/{id}', name: 'user.clientUsers', methods: ['GET'])]
    public function clientUsers(int $id, ClientRepository $clientRepo, SerializerInterface $serializer): Response
    {
        $client = $clientRepo->find($id);

        if(!$client) {
            $response = new JsonResponse(['message' => 'Le client n\'existe pas'], Response::HTTP_NOT_FOUND);
        } else {
            $users = $client->getUsers();
            $data = $serializer->serialize($users, 'json');

            $response = new Response($data, Response::HTTP_OK);
        }

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
