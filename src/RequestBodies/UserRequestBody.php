<?php

namespace App\RequestBodies;

use OpenApi\Annotations as OA;

/**
 * @OA\RequestBody(
 *     request="UserRequestBody",
 *     @OA\JsonContent(
 *         @OA\Property(type="string", property="name"),
 *         @OA\Property(type="string", property="surname"),
 *         @OA\Property(type="string", property="username"),
 *         @OA\Property(type="string", property="mail")
 *     )
 * )
 */
class UserRequestBody
{

}
