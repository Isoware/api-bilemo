<?php

namespace App\RequestBodies;

use OpenApi\Annotations as OA;

/**
 * @OA\RequestBody(
 *     request="ProductRequestBody",
 *     @OA\JsonContent(
 *         @OA\Property(type="string", property="name"),
 *         @OA\Property(type="string", property="description"),
 *         @OA\Property(type="float", property="price")
 *     )
 * )
 */
class ProductRequestBody {

}
