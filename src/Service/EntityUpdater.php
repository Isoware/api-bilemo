<?php

namespace App\Service;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class EntityUpdater
{
    private FormFactoryInterface $formFactory;

    public function __construct(FormFactoryInterface $formFactory) {
        $this->formFactory = $formFactory;
    }

    public function update(Object $object, Request $request): Object
    {
        $namespace = "App\Form\\" . (new \ReflectionClass($object))->getShortName() . "Type";

        $form = $this->formFactory->createBuilder($namespace, $object)->getForm();
        $form->submit(json_decode($request->getContent(), true), false);

        return $object;
    }
}
